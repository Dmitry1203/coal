﻿<!DOCTYPE html>
<head><title>Carbonmall Coal Trading (CCT)</title>
<link rel="icon" type="image/svg+xml" href="favicon/favicon.svg">
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="MobileOptimized" content="320">
<meta name="HandheldFriendly" content="True">
<meta name="keywords" content="Carbonmall Coal Trading (CCT)">
<meta name="description" content="Carbonmall Coal Trading (CCT) is a private trading company specializing in global long-term direct deliveries of thermal coal to target industries.">
<meta name="robots" content= "index,all">
<link rel="stylesheet" type="text/css" href="css/fonts.css">
<link rel="stylesheet" type="text/css" href="css/styles.css?<?=sha1(microtime(1))?>"> 
<link rel="stylesheet" type="text/css" href="css/main.css"> 
</head>

<body>

<section class="nav-panel p-2">***
	<div id="close-menu">&#10006;</div>
		<br><br>
		<div class="content text-center">				
			<img src = "images/logo.svg" class="logo" alt="Carbonmall Coal Trading (CCT) is a private trading company specializing in global long-term direct deliveries of thermal coal to target industries.">
			<p><a href="/" class="link-footer" id="nav-panel__main">Main</a></p>	
			<p class="m-4"><a href="company" class="link-footer">Who are we?</a></p>
			<p class="m-4"><a href="/" class="link-footer" id="nav-panel__sectors">Core industry sectors</a></p>
			<p class="m-4"><a href="/" class="link-footer" id="nav-panel__different">What make us different?</a></p>
			<p class="m-4"><a href="/" class="link-footer" id="nav-panel__contacts">Contacts</a></p>
		</div>	
</section>
<div id="top-nav">
	<span id="navbar-toggler-icon"></span>
</div>

<div id="page">

	<div id="nav-container">
		<div id="nav1" class="nav-btn curr-section"></div>
		<div id="nav2" class="nav-btn"></div>
		<div id="nav3" class="nav-btn"></div>
		<div id="nav4" class="nav-btn"></div>
		<div id="nav5" class="nav-btn"></div>
	</div>

	<div id="section1" class="section">
		<div id="section1__content" class="content vertical text-center">				
			<img src = "images/logo.svg" class="logo" alt="Carbonmall Coal Trading (CCT) is a private trading company specializing in global long-term direct deliveries of thermal coal to target industries.">
			<p class="f-light">Deliveries of thermal coal
			<br>to target industries</p>					
			<img src = "images/arr.svg" id="section1__arr">
		</div>
	</div>

	<div id="section2" class="section">
		<div id="section2__content" class="content vertical text-center">
			<img src = "images/logo.svg" class="logo--page" alt="Carbonmall Coal Trading (CCT) is a private trading company specializing in global long-term direct deliveries of thermal coal to target industries.">					
			<h1>Who are we?</h1>
			<div class="text-justify">					
				<p>Carbonmall Coal Trading (CCT) is a private trading company specializing in global long-term direct deliveries of thermal coal to target industries. The company has experience and can create value.</p>	
			</div>
			<br>
			<a href="company" class="link-btn border-radius-10">Read more</a>					
		</div>
	</div>

	<div id="section3" class="section">
		<div id="section3__content" class="content vertical text-center">
			<img src = "images/logo.svg" class="logo--page" alt="Carbonmall Coal Trading (CCT) is a private trading company specializing in global long-term direct deliveries of thermal coal to target industries.">					
			<h1>Core industry sectors</h1>
			<div class="text-justify">
				<p>The CCT company supplies coal products wholesale; both to the domestic market and for export to China, Japan, South Korea, Singapore, Hong Kong and other countries.</p>
			</div>
			<br>
			<a href="core-industry-sectors" class="link-btn border-radius-10">Read more</a>			
		</div>
	</div>

	<div id="section4" class="section">
		<div id="section4__content" class="content vertical text-center">
			<img src = "images/logo.svg" class="logo--page" alt="Carbonmall Coal Trading (CCT) is a private trading company specializing in global long-term direct deliveries of thermal coal to target industries.">					
			<h1>What make us different?</h1>
			<div class="text-justify">
				<p>Carbonmall Coal Trading (CCT) constantly delivers the specialist knowledge and regulatory expertise that is required to invest successfully in coal industries globally.</p>					
			</div>
			<br>		
			<a href="different" class="link-btn border-radius-10">Read more</a>	
		</div>
	</div>

	<div id="section5" class="section">					
		<div id="section5__content" class="content vertical text-center">
			<img src = "images/logo.svg" class="logo--page" alt="Carbonmall Coal Trading (CCT) is a private trading company specializing in global long-term direct deliveries of thermal coal to target industries.">					
			<div class="text-justify">
				<p>CARBONMALL COAL TRADING L.L.C.
				Limited Liability Company (LLC)</p>

				<p class="m-4">Office SM1-11C, 64 20th Street, Al Khabaisi, Dubai, UAE</p>

				<p class="m-4">TEL +971 522 64 00 49</p>
				<p><a href="mailTo:carbonmall@coal.ae" class="link-footer">carbonmall@coal.ae</a></p>			
				<p class="m-4">© Copyright <?=date("Y")?> coal.ae</p>
				
			</div>
		</div>			
	</div>	

</div>			

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.19/jquery.touchSwipe.min.js"></script>
<script src="js/jQueryEasing.js"></script>
<script src="js/main.js?<?=sha1(microtime(1))?>"></script>
</body>
</html>